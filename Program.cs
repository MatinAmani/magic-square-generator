﻿using System;
class program
{
	public static void Main(string[] args)
	{
		MagicSquare magicSquare0 = new MagicSquare(5);
		MagicSquare magicSquare1 = new MagicSquare(11);
		magicSquare0.Print();
		Console.WriteLine();
		magicSquare1.Print();

		Console.ReadKey();
	}
}

class MagicSquare
{
	private int size;
	private int[,] matrix;

	public MagicSquare(int n)
	{
		if (n % 2 != 0)
		{
			size = n;
			this.Generate();
		}
		else
		{
			Console.WriteLine("Matrix Size Must be Odd.");
		}
	}

	private void Generate()
	{
		int step;
		int iPtr, jPtr;
		this.matrix = new int[size, size];

		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				this.matrix[i, j] = 0;
			}
		}

		step = 1;
		iPtr = 0;
		jPtr = (this.size - 1) / 2;

		while (step <= Math.Pow(this.size, 2))
		{
			if (iPtr < 0 || jPtr < 0)
			{
				if (iPtr < 0 && jPtr < 0)
				{
					iPtr = 1;
					jPtr = 0;
					this.matrix[iPtr, jPtr] = step;
				}

				else if (iPtr < 0)
				{
					iPtr = this.size - 1;
					this.matrix[iPtr, jPtr] = step;
				}

				else if (jPtr < 0)
				{
					jPtr = this.size - 1; ;
					this.matrix[iPtr, jPtr] = step;
				}
			}

			else if (this.matrix[iPtr, jPtr] != 0)
			{
				iPtr += 2;
				jPtr++;
				this.matrix[iPtr, jPtr] = step;
			}

			else
			{
				this.matrix[iPtr, jPtr] = step;
			}

			iPtr--;
			jPtr--;
			step++;
		}
	}

	public void Print()
	{
		for (int i = 0; i < this.size; i++)
		{
			for (int j = 0; j < this.size; j++)
			{
				Console.Write($"{this.matrix[i, j]}\t");
			}
			Console.WriteLine();
		}
	}
}